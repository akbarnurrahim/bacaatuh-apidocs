const Book = require('../models/book');
const Order = require('../models/order');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

module.exports.seeAllBooks = (req, res) => {
  Book.findAll({
    attributes : ['id','isbn','title','image','author','publisher_name','publication_year','stock','language','product_dimensions','description','retail_price']
    }).then((books) => {
      res.json(books);
    });
}

module.exports.viewOneBooks = (req, res) => {
  Book.findAll({
    where: {
       id : req.params.id
    },
    attributes : ['id','isbn','title','image','author','publisher_name','publication_year','stock','language','product_dimensions','description','retail_price']
    }).then((books) => {
      res.json(books);
 });
}

module.exports.updateBooks = (req, res) => {
  jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
    if (authData['roles'] != "admin"){
      res.send("~ Maaf Halaman Ini khusus Admin !");
    } else {
  Book.update({
    isbn : req.body.isbn,
    title : req.body.title,
    number_of_page : req.body.number_of_page,
    image : req.body.image,
    author : req.body.author,
    publisher_name : req.body.publisher_name,
    stock : req.body.stock,
    language: req.body.language,
    product_dimensions : req.body.product_dimensions,
    publication_year : req.body.publication_year,
    description : req.body.description,
    retail_price : req.body.retail_price
},{
where: {
 id: req.params.id
 }
}).then((book) => {
  res.send('data berhasil diubah');
});
}
});
}


module.exports.deleteBooks = (req, res) => {
  jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
    if (authData['roles'] != "admin"){
      {
        res.json({warning: "Maaf Anda Tidak Memiliki Akses Terhadap Halaman Ini"})
      }
    } else {
      Post.destroy({
      where: {
      id: 'req.params.id'
      }
      });
    }
  })
}


module.exports.insertBooks = (req, res) => {
  jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
    if (authData['roles'] != "admin"){
      {
        res.json({warning: "Maaf Anda Tidak Memiliki Akses Terhadap Halaman Ini"})
      }
    } else {
  Book.create({
       isbn : req.body.isbn,
       title : req.body.title,
       number_of_page : req.body.number_of_page,
       image : req.body.image,
       author : req.body.author,
       publisher_name : req.body.publisher_name,
       stock : req.body.stock,
       language: req.body.language,
       product_dimensions : req.body.product_dimensions,
       publication_year : req.body.publication_year,
       description : req.body.description,
       retail_price : req.body.retail_price
    }).then((book) => {
            res.json(book);
        }).catch((error) => {
            throw error;
        })
          }
      })

    }
