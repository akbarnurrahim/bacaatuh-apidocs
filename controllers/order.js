const Book = require('../models/book');
const Order = require('../models/order');
const User = require('../models/user');

const date = require('date-utils');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();


module.exports.orderBooks = (req, res) => {
  jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
    if (authData['roles'] != "customer"){
      {
        res.json({warning: "Harap Login Sebagai Customer Untuk Melakukan pengorderan"})
      }
    } else {
  const id = authData['id'];
  Book.findOne({
   where: {
       id: req.body.id
   }
 }).then((book) => {
   console.log('aye');
   Order.create({
        quantity : req.body.quantity,
        order_date: Date.today(),
        sub_total: book.retail_price,
        total: book.retail_price * req.body.quantity,
        bookId : book.id,
        userId : id
   }).then((product) => {
     Book.findAll({
       where: {
        Id : book.id
      },
      include : [
        {
          model : Order,
          where : {
            id : product.id
          },
          attributes : ['order_date','quantity','sub_total','total']
        },

      ],
      attributes : ['id','isbn','title','author','publisher_name','retail_price']

    }).then((struk) => {
         res.json(struk);

    });
  })
})
}
})
}
