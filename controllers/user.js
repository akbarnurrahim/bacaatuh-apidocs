const bycrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();
const jwt = require('jsonwebtoken');
const User = require('../models/user');

module.exports.postRegister = (req, res) => {
    var salt = bycrypt.genSaltSync(10);
    var hash = bycrypt.hashSync(req.body.password, salt);

    User.findOrCreate({
        where: {
            email: req.body.email
        },
        defaults: {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          email: req.body.email,
          phone: req.body.phone,
          birthdate: req.body.birthdate,
          gender: req.body.gender,
          adress1: req.body.adress1,
          adress2: req.body.adress2,
          state: req.body.state,
          province: req.body.province,
          city: req.body.city,
          password: hash,
          roles : req.body.roles
        }
    }).then((user) => {
        res.json(user)
    })
    .catch((error) => {
        console.log(error);
    })
}

module.exports.postLoginAdmin = (req, res) => {
    User
        .findOne({
            where: {
                email: req.body.email,

            }
        })
        .then((user) => {
            if (!user) {
                res.status(400).send('Username not found');
            }

            bycrypt.compare(
                req.body.password,
                user.get('password'),
                (error, isMatch) => {
                    if (error) {
                        res.status(400).send('Password Match error');
                    }
                    if (isMatch) {
                        jwt.sign({id: user.get('id'), roles: user.get('roles')}, process.env.SECRETKEY, (error, token) => {
                            res.json({token: token})
                        });
                    } else {
                        res.status(400).send('Wrong password');
                    }
                })
        })
}
