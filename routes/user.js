const express = require('express');

const router = express.Router();
const auth = require('../configs/auth');

const userControllers = require('../controllers/user');
router.post('/register', userControllers.postRegister);
router.post('/login', userControllers.postLoginAdmin);


module.exports = router;
