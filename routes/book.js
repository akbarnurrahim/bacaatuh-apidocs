const express = require('express');

const router = express.Router();

const bookController = require('../controllers/book');

const auth = require('../configs/auth');

router.get('/show',bookController.seeAllBooks);
router.get('/show/:id', bookController.viewOneBooks);


router.delete('/:id',auth.verifyToken, bookController.deleteBooks);
router.post('/', auth.verifyToken,bookController.insertBooks);
router.put('/:id', auth.verifyToken,bookController.updateBooks);


module.exports = router;
