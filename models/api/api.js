const Sequelize = require('sequelize');
const sequelize = require('../../configs/sequelize');

class Api extends Sequelize.Model {}

Api.init({
  api_name: Sequelize.STRING,
  category: Sequelize.STRING,
  description: Sequelize.STRING,
  http_method: Sequelize.STRING,
  url: Sequelize.STRING,
  example: Sequelize.STRING


},{ sequelize, modelName: 'api'});

module.exports = Api;
