const Sequelize = require('sequelize');
const sequelize = require('../../configs/sequelize');

class Input extends Sequelize.Model {}

Input.init({
  type : Sequelize.STRING,
  name : Sequelize.STRING,
  action_form : Sequelize.STRING


},{ sequelize, modelName: 'input'});

module.exports = Input;
