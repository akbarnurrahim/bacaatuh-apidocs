const Sequelize = require('sequelize');
const sequelize = require('../configs/sequelize');

class User extends Sequelize.Model {}

User.init({
  first_name: Sequelize.STRING,
  last_name: Sequelize.STRING,
  email: Sequelize.STRING,
  phone: Sequelize.STRING,
  birthdate: Sequelize.DATE,
  gender: Sequelize.STRING,
  adress1: Sequelize.STRING,
  adress2: Sequelize.STRING,
  province : Sequelize.STRING,
  city: Sequelize.STRING,
  state: Sequelize.STRING,
  password: Sequelize.STRING,
  roles : Sequelize.STRING

}, { sequelize,modelName:'user'})

module.exports = User
