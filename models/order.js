const Sequelize = require('sequelize');
const sequelize = require('../configs/sequelize');

class Order extends Sequelize.Model {}

Order.init({
  order_date: Sequelize.DATE,
  quantity : Sequelize.STRING,
  sub_total: Sequelize.INTEGER,
  total: Sequelize.INTEGER
}, { sequelize, modelName:'order'});

module.exports = Order;
