const Sequelize = require('sequelize');
const sequelize = require('../configs/sequelize');

class Category extends Sequelize.Model {}

Category.init({
  category_name : Sequelize.STRING,
  category_description: Sequelize.STRING

}, { sequelize, modelName:'category'});

module.exports = Category;
