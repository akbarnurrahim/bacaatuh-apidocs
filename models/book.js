const Sequelize = require('sequelize');
const sequelize = require('../configs/sequelize');

class Book extends Sequelize.Model {}

Book.init({
  isbn: Sequelize.STRING,
  title: Sequelize.STRING,
  number_of_page: Sequelize.INTEGER,
  image: Sequelize.STRING,
  author: Sequelize.STRING,
  publisher_name: Sequelize.STRING,
  stock : Sequelize.INTEGER,
  language: Sequelize.STRING,
  product_dimensions : Sequelize.STRING,
  publication_year: Sequelize.INTEGER,
  description: Sequelize.STRING,
  retail_price: Sequelize.FLOAT

}, { sequelize, modelName: 'book'});

module.exports = Book;
