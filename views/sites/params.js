const Sequelize = require('sequelize');
const sequelize = require('../../configs/sequelize');

class Params extends Sequelize.Model {}

Params.init({
  name: Sequelize.STRING,
  data_type: Sequelize.STRING,
  required: Sequelize.BOOLEAN,
  description : Sequelize.STRING


},{ sequelize, modelName: 'params'});

module.exports = Params;
