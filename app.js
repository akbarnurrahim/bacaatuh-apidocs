const express = require('express');
const app = express();
const path = require('path');

const homeRouter = require('./routes/home');
const bookRouter = require('./routes/book');
const orderRouter = require('./routes/order');
const userRouter = require('./routes/user');


app.set('view engine','ejs');

const sequelize = require('./configs/sequelize')
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')))

const User = require('./models/user');
const Category = require('./models/category');
const Order = require('./models/order');
const Book = require('./models/book');

Book.hasMany(Order);
User.hasMany(Order);
Category.hasMany(Book);

app.use(homeRouter);
app.use('/book',bookRouter);
app.use('/order',orderRouter);
app.use('/user',userRouter);

app.listen(3000, ()=> {
  console.log('ok');
  sequelize.sync()
})
